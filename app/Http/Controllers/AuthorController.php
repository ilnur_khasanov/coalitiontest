<?php

namespace App\Http\Controllers;

use App\Http\Requests\AuthorRequest;
use App\Models\Author;
use Illuminate\Http\Request;

class AuthorController extends Controller
{
    /**
     * @api {get} /api/authors Authors list
     * @apiName AuthorsList
     * @apiGroup Author
     *
     * @apiParam {Number} offset Offset
     * @apiParam {Number} limit Limit
     * @apiParam {String} [phrase] Phrase
     */
    public function index(Request $request)
    {
        $authors = Author::withCount('books');

        if (!empty($request->phrase)) {
            $authors = $authors->where(function($query) use ($request) {
                $query->where('name', 'like', '%' . $request->phrase . '%')
                    ->orWhere('surname', 'like', '%' . $request->phrase . '%')
                    ->orWhere('patonymic', 'like', '%' . $request->phrase . '%');
            });
        }

        return $authors->offset($request->offset)
            ->limit($request->limit)
            ->get();
    }

    /**
     * @api {post} /api/authors Authors store
     * @apiName AuthorsStore
     * @apiGroup Author
     *
     * @apiParam {String} name Name
     * @apiParam {String} surname Surname
     * @apiParam {String} [patonymic] Patonymic
     * @apiParam {Number} birth Birth
     * @apiParam {Number} [death] Death
     */
    public function store(AuthorRequest $request)
    {
        return Author::create($request->validated());
    }

    /**
     * @api {get} /api/authors Authors show
     * @apiName AuthorsShow
     * @apiGroup Author
     *
     * @apiParam {Number} id Id
     */
    public function show(Author $author)
    {
        return Author::withCount('books')->findOrFail($author->id);
    }

    /**
     * @api {put} /api/authors Authors update
     * @apiName AuthorsUpdate
     * @apiGroup Author
     *
     * @apiParam {Number} id Id
     * @apiParam {String} name Name
     * @apiParam {String} surname Surname
     * @apiParam {String} [patonymic] Patonymic
     * @apiParam {Number} birth Birth
     * @apiParam {Number} [death] Death
     */
    public function update(AuthorRequest $request, $id)
    {
        $author = Author::findOrFail($id);
        $author->fill($request->except(['id']));
        $author->save();

        return response()->json($author);
    }

    /**
     * @api {delete} /api/authors Authors delete
     * @apiName AuthorsDelete
     * @apiGroup Author
     *
     * @apiParam {Number} id Id
     */
    public function destroy($id)
    {
        $author = Author::findOrFail($id);
        if ($author->delete()) {
            return response(null, 204);
        }
    }
}
