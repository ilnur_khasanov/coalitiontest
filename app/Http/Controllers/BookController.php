<?php

namespace App\Http\Controllers;

use App\Models\Book;
use Illuminate\Http\Request;

class BookController extends Controller
{
    /**
     * @api {get} /api/books Books list
     * @apiName BooksList
     * @apiGroup Book
     *
     * @apiParam {Number} offset Offset
     * @apiParam {Number} limit Limit
     * @apiParam {String} [phrase] Phrase
     */
    public function index(Request $request)
    {
        $books = Book::with('authors');

        if (!empty($request->phrase)) {
            $books = $books->where(function($query) use ($request) {
                $query->where('title', 'like', '%' . $request->phrase . '%')
                    ->orWhere('description', 'like', '%' . $request->phrase . '%');
            });
        }

        return $books->offset($request->offset)
            ->limit($request->limit)
            ->get();
    }

    /**
     * @api {post} /api/books Books store
     * @apiName BooksStore
     * @apiGroup Book
     *
     * @apiParam {String} title Title
     * @apiParam {String} [description] Description
     * @apiParam {Number} published_at PublishedAt
     */
    public function store(Request $request)
    {
        return Book::create($request->validated());
    }

    /**
     * @api {get} /api/books Books show
     * @apiName BooksShow
     * @apiGroup Book
     *
     * @apiParam {Number} id Id
     */
    public function show(Book $book)
    {
        return Book::withCount('books')->findOrFail($book->id);
    }

    /**
     * @api {post} /api/books Books update
     * @apiName BooksUpdate
     * @apiGroup Book
     *
     * @apiParam {Number} id Id
     * @apiParam {String} title Title
     * @apiParam {String} [description] Description
     * @apiParam {Number} published_at PublishedAt
     */
    public function update(Request $request, $id)
    {
        $book = Book::findOrFail($id);
        $book->fill($request->except(['id']));
        $book->save();

        return response()->json($book);
    }

    /**
     * @api {delete} /api/books Books delete
     * @apiName BooksDelete
     * @apiGroup Book
     *
     * @apiParam {Number} id Id
     */
    public function destroy($id)
    {
        $author = Book::findOrFail($id);
        if ($author->delete()) {
            return response(null, 204);
        }
    }
}
