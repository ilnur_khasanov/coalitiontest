<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Request;

class AuthorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        $rules = [
            'name' => 'required|string',
            'surname' => 'required|string',
            'patonymic' => 'required|string',
            'birth' => 'required|numeric',
            'death' => 'numeric',
        ];

        switch ($this->getMethod()) {
            case 'POST':
                return $rules;
            case 'PUT':
                return ['id' => 'required|integer|exists:authors,id',] + $rules;
            case 'DELETE':
                return ['id' => 'required|integer|exists:authors,id',];
        }
    }
}
