<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Author extends Model
{
    use HasFactory;

    protected $table = 'authors';
    protected $fillable = [
        'id',
        'name',
        'surname',
        'patonymic',
        'birth',
        'death',
        'created_at',
        'updated_at',
    ];

    public function books() {
        return $this->belongsToMany(Book::class, 'author_book', 'author_id', 'book_id');
    }

    /**
     * Get books count.
     *
     * @return integer
     */
    public function booksCount() {
        return $this->books->sum('pivot.id');
    }
}
