<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    use HasFactory;

    protected $table = 'books';
    protected $fillable = [
        'id',
        'title',
        'description',
        'published_at',
        'created_at',
        'updated_at',
    ];

    public function authors() {
        return $this->belongsToMany(Author::class, 'author_book', 'book_id', 'author_id');
    }
}
