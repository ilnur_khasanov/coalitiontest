<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAuthorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('authors')) {
            Schema::create('authors', function (Blueprint $table) {
                $table->id();
                $table->string('name')->nullable()->default(null)->comment('Имя автора');
                $table->string('surname')->nullable()->default(null)->comment('Фамилия автора');
                $table->string('patonymic')->nullable()->default(null)->comment('Отчество автора');
                $table->integer('birth')->nullable()->default(null)->comment('Год рождения');
                $table->integer('death')->nullable()->default(null)->comment('Год смерти');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('authors');
    }
}
