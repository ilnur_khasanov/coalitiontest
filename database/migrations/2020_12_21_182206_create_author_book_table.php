<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAuthorBookTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('author_book')) {
            Schema::create('author_book', function (Blueprint $table) {
                $table->id();
                $table->integer('author_id')->unsigned();
                $table->foreign('author_id')->references('id')->on('authors');
                $table->integer('book_id')->unsigned();
                $table->foreign('book_id')->references('id')->on('books');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('author_book');
    }
}
