<?php

namespace Database\Seeders;

use App\Models\Author;
use App\Models\Book;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Book::factory()->count(20)->create();
        Author::factory()->count(50)->create();
        $books = Book::all();

        Author::all()->each(function ($author) use ($books) {
            $author->roles()->attach(
                $books->random(rand(1, 3))->pluck('id')->toArray()
            );
        });
    }
}
