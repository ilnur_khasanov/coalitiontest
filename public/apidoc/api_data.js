define({ "api": [
  {
    "type": "delete",
    "url": "/api/authors",
    "title": "Authors delete",
    "name": "AuthorsDelete",
    "group": "Author",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Id</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "app/Http/Controllers/AuthorController.php",
    "groupTitle": "Author"
  },
  {
    "type": "get",
    "url": "/api/authors",
    "title": "Authors list",
    "name": "AuthorsList",
    "group": "Author",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "offset",
            "description": "<p>Offset</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "limit",
            "description": "<p>Limit</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "phrase",
            "description": "<p>Phrase</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "app/Http/Controllers/AuthorController.php",
    "groupTitle": "Author"
  },
  {
    "type": "get",
    "url": "/api/authors",
    "title": "Authors show",
    "name": "AuthorsShow",
    "group": "Author",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Id</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "app/Http/Controllers/AuthorController.php",
    "groupTitle": "Author"
  },
  {
    "type": "post",
    "url": "/api/authors",
    "title": "Authors store",
    "name": "AuthorsStore",
    "group": "Author",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Name</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "surname",
            "description": "<p>Surname</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "patonymic",
            "description": "<p>Patonymic</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "birth",
            "description": "<p>Birth</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "death",
            "description": "<p>Death</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "app/Http/Controllers/AuthorController.php",
    "groupTitle": "Author"
  },
  {
    "type": "put",
    "url": "/api/authors",
    "title": "Authors update",
    "name": "AuthorsUpdate",
    "group": "Author",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Id</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Name</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "surname",
            "description": "<p>Surname</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "patonymic",
            "description": "<p>Patonymic</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "birth",
            "description": "<p>Birth</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "death",
            "description": "<p>Death</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "app/Http/Controllers/AuthorController.php",
    "groupTitle": "Author"
  },
  {
    "type": "delete",
    "url": "/api/books",
    "title": "Books delete",
    "name": "BooksDelete",
    "group": "Book",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Id</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "app/Http/Controllers/BookController.php",
    "groupTitle": "Book"
  },
  {
    "type": "get",
    "url": "/api/books",
    "title": "Books list",
    "name": "BooksList",
    "group": "Book",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "offset",
            "description": "<p>Offset</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "limit",
            "description": "<p>Limit</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "phrase",
            "description": "<p>Phrase</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "app/Http/Controllers/BookController.php",
    "groupTitle": "Book"
  },
  {
    "type": "get",
    "url": "/api/books",
    "title": "Books show",
    "name": "BooksShow",
    "group": "Book",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Id</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "app/Http/Controllers/BookController.php",
    "groupTitle": "Book"
  },
  {
    "type": "post",
    "url": "/api/books",
    "title": "Books store",
    "name": "BooksStore",
    "group": "Book",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "title",
            "description": "<p>Title</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "description",
            "description": "<p>Description</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "published_at",
            "description": "<p>PublishedAt</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "app/Http/Controllers/BookController.php",
    "groupTitle": "Book"
  },
  {
    "type": "post",
    "url": "/api/books",
    "title": "Books update",
    "name": "BooksUpdate",
    "group": "Book",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Id</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "title",
            "description": "<p>Title</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "description",
            "description": "<p>Description</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "published_at",
            "description": "<p>PublishedAt</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "app/Http/Controllers/BookController.php",
    "groupTitle": "Book"
  }
] });
